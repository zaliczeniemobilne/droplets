var CANVAS_WIDTH = 400;
var CANVAS_HEIGHT = 600;
var PLAY_AREA_WIDTH = 300;
var SCORE_AREA_WIDTH = 100;
var COLOR_RED = 0;
var COLOR_YELLOW = 1;
var COLOR_GREEN = 2;
var COLOR_BLUE = 3;

var canvas;
var g;
var score = 0;
var fails = 0;
var animationInterval = null;
var bucketPositionInterval = null;
var dropletsGenerateInterval = null;
var dropletsPositionInterval = null;
var bucketColorChangeInterval = null;
var FPS = 25;
var FRAME_RATE = 1000 / FPS;
var gameStop = false;
var bucket;
var DROPLETS_INTERVAL = 1500;
var DROPLETS_POSITION_INTERVAL = 60;
var dropletsArray = new Array();

function moveLeft() {
    bucket.setDirection(DIRECTION_LEFT);
}

function moveRight() {
    bucket.setDirection(DIRECTION_RIGHT);
}

function stopLeft() {
    if(bucket.getDirection() === DIRECTION_LEFT) {
        bucket.setDirection(DIRECTION_NONE);
    }
}

function stopRight() {
    if(bucket.getDirection() === DIRECTION_RIGHT) {
        bucket.setDirection(DIRECTION_NONE);
    }
}