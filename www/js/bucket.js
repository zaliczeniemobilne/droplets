var RED_BUCKET = new Image();
var YELLOW_BUCKET = new Image();
var GREEN_BUCKET = new Image();
var BLUE_BUCKET = new Image();
var DIRECTION_NONE = 0;
var DIRECTION_LEFT = 1;
var DIRECTION_RIGHT = 2;

RED_BUCKET.src = "img/red_bucket.png";
YELLOW_BUCKET.src = "img/yellow_bucket.png";
GREEN_BUCKET.src = "img/green_bucket.png";
BLUE_BUCKET.src = "img/blue_bucket.png";

function Bucket() {
    this.color = COLOR_RED;
    this.x = CANVAS_WIDTH / 2 - 20;
    this.y = CANVAS_HEIGHT - 60;
    this.image = RED_BUCKET;
    this.direction = DIRECTION_NONE;
}

Bucket.prototype.getColor = function () {
    return this.color;
}

Bucket.prototype.getX = function () {
    return this.x;
}

Bucket.prototype.getY = function () {
    return this.y;
}

Bucket.prototype.getImage = function () {
    return this.image;
}

Bucket.prototype.getDirection = function() {
    return this.direction;
}

Bucket.prototype.setX = function (x) {
    if (x > CANVAS_WIDTH || x < 0) {
        return;
    }

    this.x = x;
}

Bucket.prototype.setDirection = function(direction) {
    if(direction < DIRECTION_NONE || direction > DIRECTION_RIGHT) {
        return;
    }

    this.direction = direction;
}

Bucket.prototype.setColor = function (color) {
    if (color < COLOR_RED || color > COLOR_BLUE) {
        return;
    }

    this.color = color;

    switch (color) {
        case COLOR_RED:
            this.image = RED_BUCKET;
            break;
        case COLOR_YELLOW:
            this.image = YELLOW_BUCKET;
            break;
        case COLOR_GREEN:
            this.image = GREEN_BUCKET;
            break;
        case COLOR_BLUE:
            this.image = BLUE_BUCKET;
            break;
    }
}