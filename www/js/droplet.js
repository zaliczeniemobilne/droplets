var RED_DROPLET = new Image();
var YELLOW_DROPLET = new Image();
var GREEN_DROPLET = new Image();
var BLUE_DROPLET = new Image();

RED_DROPLET.src = "img/red_bucket.png";
YELLOW_DROPLET.src = "img/yellow_bucket.png";
GREEN_DROPLET.src = "img/green_bucket.png";
BLUE_DROPLET.src = "img/blue_bucket.png";

function Droplet() {
    this.color = Math.floor(Math.random() * 5);

    if(this.color === 4) {
        this.color = 0;
    }

    this.x = Math.floor(Math.random() * (CANVAS_WIDTH - 70) + 30);
    this.y = 30;

    switch (this.color) {
        case COLOR_RED:
            this.image = RED_DROPLET;
            break;
        case COLOR_YELLOW:
            this.image = YELLOW_DROPLET;
            break;
        case COLOR_GREEN:
            this.image = GREEN_DROPLET;
            break;
        case COLOR_BLUE:
            this.image = BLUE_DROPLET;
            break;
    }
}

Droplet.prototype.getColor = function () {
    return this.color;
}

Droplet.prototype.getX = function () {
    return this.x;
}

Droplet.prototype.getY = function () {
    return this.y;
}

Droplet.prototype.getImage = function () {
    return this.image;
}

Droplet.prototype.setY = function (y) {
    if (y < 0 || y > CANVAS_HEIGHT) {
        return;
    }

    this.y = y;
}